//
//  HomeTableViewController.m
//  StudyRoute
//
//  Created by Vinicius Andrade on 12/12/15.
//  Copyright © 2015 Vinicius Andrade. All rights reserved.
//

#import "HomeTableViewController.h"
#import "RouteDetailViewController.h"
#import "HomeTableViewCell.h"
#import "RouteDAO.h"

@interface HomeTableViewController ()

@property (nonatomic, strong) NSMutableArray *routesDataSource;

@end

@implementation HomeTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
    
    self.routesDataSource = [RouteDAO selectRoutes];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.routesDataSource = [RouteDAO selectRoutes];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)goneBack:(UIStoryboardSegue *)segue {
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.routesDataSource.count;
}

- (HomeTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"homeCell"
                                                              forIndexPath:indexPath];
    
    Route *route = self.routesDataSource[indexPath.row];
    
    cell.titleLabel.text = route.name;
    cell.subtitleLabel.text = [NSString stringWithFormat:@"%ld", (unsigned long)route.questions.count];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [RouteDAO deleteRoute:self.routesDataSource[indexPath.row]];
        [self.routesDataSource removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"openRoute" sender:self.routesDataSource[indexPath.row]];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80.0f;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"openRoute"]) {
        RouteDetailViewController *routeDetail = [segue destinationViewController];
        
        routeDetail.route = sender;
    }
}

@end
