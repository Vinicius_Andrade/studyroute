//
//  NewViewController.h
//  StudyRoute
//
//  Created by Vinicius Andrade on 13/02/16.
//  Copyright © 2016 Vinicius Andrade. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RouteDAO.h"

@interface NewViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *nameTextField;
@property (strong, nonatomic) IBOutlet UITextField *questionTextField;
@property (strong, nonatomic) IBOutlet UITextField *answerTextField;
@property (strong, nonatomic) Route *route;
@property (strong, nonatomic) IBOutlet UILabel *labelNumber;
@property int index;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)addButton:(id)sender;
- (IBAction)undoButton:(id)sender;
- (IBAction)doneButton:(id)sender;

@end
