//
//  Route.m
//  StudyRoute
//
//  Created by Vinicius Andrade on 16/02/16.
//  Copyright © 2016 Vinicius Andrade. All rights reserved.
//

#import "Route.h"

@implementation Route

- (id)init {
    self = [super init];
    
    if (self) {
        self.questions = [[NSMutableArray alloc]init];
        self.answers = [[NSMutableArray alloc]init];
    }
    
    return self;
}

@end
