//
//  Route.h
//  StudyRoute
//
//  Created by Vinicius Andrade on 16/02/16.
//  Copyright © 2016 Vinicius Andrade. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Route : NSObject

@property (strong, nonatomic) NSMutableArray *questions;
@property (strong, nonatomic) NSMutableArray *answers;
@property (strong, nonatomic) NSString *routeID;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *lastPlayed;
@property int timesPlayed;

@end
