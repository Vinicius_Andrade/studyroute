//
//  HomeTableViewController.h
//  StudyRoute
//
//  Created by Vinicius Andrade on 12/12/15.
//  Copyright © 2015 Vinicius Andrade. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface HomeTableViewController : UITableViewController

@end
