//
//  RouteDetailViewController.m
//  StudyRoute
//
//  Created by Vinicius Andrade on 14/02/16.
//  Copyright © 2016 Vinicius Andrade. All rights reserved.
//

#import "RouteDetailViewController.h"
#import "PlayingViewController.h"

@interface RouteDetailViewController ()

@end

@implementation RouteDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.route.name;
    
    self.tableView.tableFooterView = [[UIView alloc]initWithFrame:CGRectZero];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self carregaLabels];
}

- (void)carregaLabels
{
    self.questionsLabel.text = [NSString stringWithFormat:@"%li", (unsigned long)self.route.questions.count];
    self.timesPlayedLabel.text = [NSString stringWithFormat:@"%i", self.route.timesPlayed];
    self.lastPlayedLabel.text = NSLocalizedString(self.route.lastPlayed, nil);
    
    self.editButton.title = NSLocalizedString(@"Edit", nil);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.route.questions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"CellDetail"];
    
    cell.textLabel.text = self.route.questions[indexPath.row];
    cell.detailTextLabel.text = self.route.answers[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [RouteDAO deleteQuestion:self.route.questions[indexPath.row] andAnswer:self.route.answers[indexPath.row] onRouteID:self.route.routeID];
        [self.route.questions removeObjectAtIndex:indexPath.row];
        [self.route.answers removeObjectAtIndex:indexPath.row];
        if (self.route.questions.count == 0) {
            [RouteDAO deleteRoute:self.route];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else {
            [self carregaLabels];
            [self.tableView reloadData];
        }
    }
}

- (IBAction)begin:(id)sender {
    self.route.timesPlayed++;
    [RouteDAO updatedTimesPlayed:[NSNumber numberWithInt:self.route.timesPlayed] onRouteID:self.route.routeID];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/YYYY hh:mm"];
    self.route.lastPlayed = [formatter stringFromDate:[NSDate date]];
    [RouteDAO lastPlayed:self.route.lastPlayed onRouteID:self.route.routeID];
    [self performSegueWithIdentifier:@"begun" sender:nil];
}

- (IBAction)edit:(id)sender {
    if (self.tableView.isEditing) {
        [self.tableView setEditing:NO animated:YES];
        self.editButton.title = NSLocalizedString(@"Edit", nil);
    } else {
        [self.tableView setEditing:YES animated:YES];
        self.editButton.title = NSLocalizedString(@"Done", nil);
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"begun"]) {
        PlayingViewController *vc = segue.destinationViewController;
        
        vc.questions = self.route.questions;
        vc.answers = self.route.answers;
        
        vc.title = self.title;
    }
}

@end
