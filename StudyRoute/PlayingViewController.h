//
//  PlayingViewController.h
//  StudyRoute
//
//  Created by Vinicius Andrade on 15/02/16.
//  Copyright © 2016 Vinicius Andrade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayingViewController : UIViewController <UIGestureRecognizerDelegate>

@property (strong, nonatomic) IBOutlet UILabel *outOfLabel;
@property (strong, nonatomic) IBOutlet UILabel *questionLabel;
@property (strong, nonatomic) IBOutlet UILabel *answerLabel;
@property (strong, nonatomic) NSArray *questions;
@property (strong, nonatomic) NSArray *answers;

@end
