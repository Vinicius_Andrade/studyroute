//
//  NewViewController.m
//  StudyRoute
//
//  Created by Vinicius Andrade on 13/02/16.
//  Copyright © 2016 Vinicius Andrade. All rights reserved.
//

#import "NewViewController.h"

@interface NewViewController ()

@end

@implementation NewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.index = 0;
    
    [self textFieldDelegates];
    
    self.route = [[Route alloc]init];
}

- (void)addQuestion {
    [self.route.questions addObject:self.questionTextField.text];
    [self.route.answers addObject:self.answerTextField.text];
    
    self.questionTextField.text = @"";
    self.answerTextField.text = @"";
    
    [self changeLabelAdding:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)addButton:(id)sender {
    if ([self filledTextFields]) {
        [self addQuestion];
    } else {
        [Constants alertWithTitle:NSLocalizedString(@"Attention", nil)
                          message:NSLocalizedString(@"Fill all the fields", nil)
                            andOK:@"OK"
                 onViewController:self];
    }
}

- (IBAction)undoButton:(id)sender {
    if (self.route.questions.count != 0) {
        [self.route.questions removeLastObject];
        [self.route.answers removeLastObject];
        
        [self changeLabelAdding:NO];
    }
}

- (IBAction)doneButton:(id)sender {
    if ([self.nameTextField.text isEqualToString:@""]) {
        [Constants alertWithTitle:NSLocalizedString(@"Attention", nil)
                          message:NSLocalizedString(@"The Route needs a name", nil)
                            andOK:@"OK"
                 onViewController:self];
        
    } else if (self.route.questions.count == 0) {
        [Constants alertWithTitle:NSLocalizedString(@"Attention", nil)
                          message:NSLocalizedString(@"Add questions before saving", nil)
                            andOK:@"OK"
                 onViewController:self];
    } else {
        [self performSegueWithIdentifier:@"goneBack" sender:nil];
    }
}

- (void)changeLabelAdding:(BOOL)adding {
    CATransition *animation = [CATransition animation];
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.type = kCATransitionPush;
    animation.subtype = adding ? kCATransitionFromBottom : kCATransitionFromTop;
    animation.duration = 0.25;
    [self.labelNumber.layer addAnimation:animation forKey:@"kCATransitionFromBottom"];
    
    if (self.route.questions.count == 1) {
        self.labelNumber.text = [NSString stringWithFormat:@"%li %@", (unsigned long)self.route.questions.count, NSLocalizedString(@"question", nil)];
    } else {
        self.labelNumber.text = [NSString stringWithFormat:@"%li %@", (unsigned long)self.route.questions.count,  NSLocalizedString(@"questions", nil)];
    }
}

- (void)unwindForSegue:(UIStoryboardSegue *)unwindSegue towardsViewController:(UIViewController *)subsequentVC {
    self.route.name = self.nameTextField.text;
    
    [RouteDAO insertNewRoute:self.route];
}

#pragma mark - Keyboard

- (void)textFieldDelegates {
    self.nameTextField.delegate = self;
    self.nameTextField.tag = 1;
    self.questionTextField.delegate = self;
    self.questionTextField.tag = 2;
    self.answerTextField.delegate = self;
    self.answerTextField.tag = 3;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField.tag == 1) {
        self.navigationItem.title = [textField.text stringByReplacingCharactersInRange:range
                                                                            withString:string];
    }
    
    if ([self.navigationItem.title isEqualToString:@""]) {
        self.navigationItem.title = @"New Route";
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.tag == 2) {
        [textField resignFirstResponder];
        [self.answerTextField becomeFirstResponder];
    }else if (textField.tag == 3) {
        [self addQuestion];
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (BOOL)filledTextFields {
    return !([self.questionTextField.text isEqualToString:@""] || [self.answerTextField.text isEqualToString:@""]);
}

@end
