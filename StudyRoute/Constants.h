//
//  Constants.h
//  StudyRoute
//
//  Created by Vinicius Andrade on 09/04/16.
//  Copyright © 2016 Vinicius Andrade. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "FMDB.h"

@interface Constants : NSObject

+ (NSString *)getDBPath;
+ (FMDatabase *)openDatabase;
+ (void)alertWithTitle:(NSString *)title message:(NSString *)message andOK:(NSString *)ok onViewController:(UIViewController *)viewController;

@end
