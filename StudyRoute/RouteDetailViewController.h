//
//  RouteDetailViewController.h
//  StudyRoute
//
//  Created by Vinicius Andrade on 14/02/16.
//  Copyright © 2016 Vinicius Andrade. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RouteDAO.h"

@interface RouteDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UILabel *questionsLabel;
@property (strong, nonatomic) IBOutlet UILabel *lastPlayedLabel;
@property (strong, nonatomic) IBOutlet UILabel *timesPlayedLabel;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *editButton;

@property (strong, nonatomic) Route *route;

- (IBAction)begin:(id)sender;
- (IBAction)edit:(id)sender;

@end
