//
//  HomeTableViewCell.h
//  StudyRoute
//
//  Created by Vinicius Andrade on 12/12/15.
//  Copyright © 2015 Vinicius Andrade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *subtitleLabel;


@end
