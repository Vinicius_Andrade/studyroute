//
//  Constants.m
//  StudyRoute
//
//  Created by Vinicius Andrade on 09/04/16.
//  Copyright © 2016 Vinicius Andrade. All rights reserved.
//

#import "Constants.h"

@implementation Constants

+ (NSString *)getDBPath
{
    NSString *docsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *dbPath   = [docsPath stringByAppendingPathComponent:@"StudyRoute.db"];
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSLog(@"%@", dbPath);
    });
    
    return dbPath;
}

+ (FMDatabase *)openDatabase
{
    NSFileManager *fm = [NSFileManager defaultManager];
    
    NSString *template_path = [[NSBundle mainBundle] pathForResource:@"StudyRoute" ofType:@"db"];
    
    if (![fm fileExistsAtPath:[Constants getDBPath]])
        [fm copyItemAtPath:template_path toPath:[Constants getDBPath] error:nil];
    FMDatabase *db = [FMDatabase databaseWithPath:[Constants getDBPath]];
    
    if (![db open])
        NSLog(@"Failed to open database!");
    
    return db;
}

+ (void)alertWithTitle:(NSString *)title message:(NSString *)message andOK:(NSString *)ok onViewController:(UIViewController *)viewController
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *actionOK = [UIAlertAction actionWithTitle:ok
                                                 style:UIAlertActionStyleDefault
                                               handler:nil];
    
    [alert addAction:actionOK];
    
    [viewController presentViewController:alert animated:YES completion:nil];
}

@end
