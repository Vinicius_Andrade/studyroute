//
//  RouteDAO.h
//  StudyRoute
//
//  Created by Vinicius Andrade on 09/04/16.
//  Copyright © 2016 Vinicius Andrade. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "FMDB.h"
#import "Route.h"

@interface RouteDAO : NSObject

+ (void)insertNewRoute:(Route *)route;
+ (void)deleteRoute:(Route *)route;
+ (void)deleteQuestion:(NSString *)question andAnswer:(NSString *)answer onRouteID:(NSString *)routeID;
+ (void)updatedTimesPlayed:(NSNumber *)times onRouteID:(NSString *)routeID;
+ (void)lastPlayed:(NSString *)date onRouteID:(NSString *)routeID;

+ (NSMutableArray *)selectRoutes;

@end
