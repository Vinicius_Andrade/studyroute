//
//  RouteDAO.m
//  StudyRoute
//
//  Created by Vinicius Andrade on 09/04/16.
//  Copyright © 2016 Vinicius Andrade. All rights reserved.
//

#import "RouteDAO.h"

@implementation RouteDAO

+ (void)insertNewRoute:(Route *)route {
    FMDatabase *db = [Constants openDatabase];
    
    [db executeUpdate:@"insert into route (routename, lastPlayed, timesPlayed) values (?, ?, ?)", route.name, @"Never", 0];
    
    [db executeUpdate:@"update route set questionsid = (select routeid from route where routename = ?), answersid = (select routeid from route where routename = ?) where routename = ?", route.name, route.name, route.name];
    
    FMResultSet *rs = [db executeQuery:@"select routeid from route where routename = ?", route.name];
    
    if ([rs next]) {
        for (NSString *question in route.questions) {
            [db executeUpdate:@"insert into question (routeid, question) values (?, ?)", [rs stringForColumn:@"routeid"], question];
        }
        for (NSString *answer in route.answers) {
            [db executeUpdate:@"insert into answer (routeid, answer) values (?, ?)", [rs stringForColumn:@"routeid"], answer];
        }
    }
    
    [rs close];
    
    [db close];
}

+ (NSMutableArray *)selectRoutes {
    FMDatabase *db = [Constants openDatabase];
    
    NSMutableArray *routes = [[NSMutableArray alloc]init];
    
    FMResultSet *rs = [db executeQuery:@"select * from route order by routeid asc"];
    
    while ([rs next]) {
        Route *route = [[Route alloc]init];
        
        route.name = [rs stringForColumn:@"routename"];
        route.routeID = [rs stringForColumn:@"routeid"];
        route.lastPlayed = [rs stringForColumn:@"lastplayed"];
        route.timesPlayed = [rs intForColumn:@"timesplayed"];
        
        FMResultSet *rss = [db executeQuery:@"select * from question where routeid = ? order by ordid asc", route.routeID];
        
        while ([rss next]) {
            [route.questions addObject:[rss stringForColumn:@"question"]];
        }
        
        rss = [db executeQuery:@"select * from answer where routeid = ? order by ordid asc", route.routeID];
        
        while ([rss next]) {
            [route.answers addObject:[rss stringForColumn:@"answer"]];
        }
        
        [rss close];
        
        [routes addObject:route];
    }
    
    [rs close];
    
    [db close];
    
    return routes;
}

+ (void)deleteRoute:(Route *)route {
    FMDatabase *db = [Constants openDatabase];
    
    [db executeUpdate:@"delete from answer where routeid = ?", route.routeID];
    
    [db executeUpdate:@"delete from question where routeid = ?", route.routeID];
    
    [db executeUpdate:@"delete from route where routeid = ?", route.routeID];
    
    [db close];
}

+ (void)deleteQuestion:(NSString *)question andAnswer:(NSString *)answer onRouteID:(NSString *)routeID {
    FMDatabase *db = [Constants openDatabase];
    
    [db executeUpdate:@"delete from answer where answer = ? and routeid = ?", answer, routeID];
    
    [db executeUpdate:@"delete from question where question = ? and routeid = ?", question, routeID];
    
    [db close];
}

+ (void)updatedTimesPlayed:(NSNumber *)times onRouteID:(NSString *)routeID {
    FMDatabase *db = [Constants openDatabase];
    
    [db executeUpdate:@"update route set timesplayed=? where routeid=?", times, routeID];
    
    [db close];
}

+ (void)lastPlayed:(NSString *)date onRouteID:(NSString *)routeID {
    FMDatabase *db = [Constants openDatabase];
    
    [db executeUpdate:@"update route set lastplayed=? where routeid=?", date, routeID];
    
    [db close];
}

@end