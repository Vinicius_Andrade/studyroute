//
//  PlayingViewController.m
//  StudyRoute
//
//  Created by Vinicius Andrade on 15/02/16.
//  Copyright © 2016 Vinicius Andrade. All rights reserved.
//

#import "PlayingViewController.h"

int indexOfArray;

@interface PlayingViewController ()

@property (strong, nonatomic) UITapGestureRecognizer *touch;

@end

@implementation PlayingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Playing...", nil);
    
    indexOfArray = 0;
    
    [self fulfillLabels];
    
    self.touch = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTouch)];
    self.touch.delegate = self;
    
    [self.view addGestureRecognizer:self.touch];
}

- (void)fulfillLabels {
    self.questionLabel.text = self.questions[indexOfArray];
    self.answerLabel.text = self.answers[indexOfArray];
    self.outOfLabel.text = [NSString stringWithFormat:@"%i %@ %li", indexOfArray+1, NSLocalizedString(@"out of", nil), (unsigned long)self.questions.count];
}

- (void)onTouch {
    if (self.questions.count-1 > indexOfArray) {
        indexOfArray++;
    } else {
        indexOfArray = 0;
    }
    
    [UIView animateWithDuration:1.0f animations:
     ^{
         self.questionLabel.alpha = 0.0f;
         self.answerLabel.alpha = 0.0f;
         
         [self fulfillLabels];
     }];
    
    [UIView animateWithDuration:1.0f animations:
     ^{
         self.questionLabel.alpha = 1.0f;
         self.answerLabel.alpha = 1.0f;
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
